#! /bin/bash

# Define colour codes
CYAN='\033[0;36m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Get absolute path to this file
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

# Create backup if file exists and is not already a symlink
function backupFile()
{
    if [ -f "$1" ] && [ ! -L "$1" ]
    then echo -e "${CYAN}Backup $1${NC}"; mv "$1" "$1_bak_$(date +%H%M%S)"
    fi
}

# Create symlink in home to file in dotfiles folder
function createSymlink()
{
    if [ ! -f ~/"$1" ]
    then echo  -e "${CYAN}Create symlink for $1${NC}"; cp -s "$SCRIPTPATH"/"$1" ~/"$1"
    fi
}

# Check if command exists
function command_exists() {
  command -v "$@" >/dev/null 2>&1
}

#######################################
# Start of actual installation script #

# install zsh
if ! command_exists zsh; then
    echo -e "${CYAN}Install zsh.${NC}"
    sudo apt -y install zsh
else
    echo -e "${CYAN}zsh is already installed.${NC}"
fi

# install oh-my-zsh if it is not installed yet
if [ ! -d "$ZSH" ]; then
    echo -e "${CYAN}Install oh-my-zsh if it not installed already.${NC}"
    echo -e "${RED}You have to re-run this script after oh-my-zsh is installed!${NC}"
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
else
    echo -e "${CYAN}oh-my-zsh is already installed.${NC}"
fi

# list all files that should be symlinked to the home folder
files=( ".zshrc" ".nanorc" ".oh-my-zsh/custom/themes/myrobbyrussel.zsh-theme" ".oh-my-zsh/custom/aliases.zsh"  ".oh-my-zsh/custom/auto-cd.zsh"  ".oh-my-zsh/custom/beep.zsh" )

for file in "${files[@]}"
do
    backupFile ~/$file
    createSymlink $file
done

# install zsh-syntax-highlighting and zsh-autosuggestions
echo  -e "${CYAN}Install zsh-syntax-highlighting${NC}"
path=${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git "$path" 2> /dev/null || (cd "$path"; git pull)
echo  -e "${CYAN}Install zsh-autosuggestions${NC}"
path=${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-autosuggestions "$path" 2> /dev/null || (cd "$path"; git pull)

echo  -e "${CYAN}All done. Start a new shell or run 'omz reload' for changes to take effect.${NC}"
