# dotfiles

Collection of config dotfiles to sync settings across machines.

Files will be symlinked into the respective folders. Should a file with the same name be present already, a backup will be created.

## Requesites

None. The script will try to install `zsh` and `oh-my-zsh` automatically before installing any user settings.
