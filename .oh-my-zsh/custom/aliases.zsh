alias zshconfig="nano ~/.zshrc"
alias sudo="sudo "
alias cp="rsync -ahE --info=progress2 --partial --stats"
alias backup="rsync --del -hrltDvu --modify-window=1 --info=progress2 --partial -e 'ssh -p 22227' --exclude '.@__thumb' --exclude '@Recycle' --exclude '.streams'"
