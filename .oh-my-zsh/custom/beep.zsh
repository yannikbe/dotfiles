# Beeping aliases (only works on WSL!)
_beep () {
  powershell.exe "[console]::beep($1,$2)"
}
alias bleep="_beep 1000 800"  # A strong bleep (for profanity)
alias  beep="_beep 2000 300"  # Quick yet noticeable beep
alias  blip="_beep 4000  80"  # A less distracting blip
alias alert="_beep 600 300; _beep 400 300; _beep 600 300;"
