# automatically 'ls' after each 'cd'
function cd {
    builtin cd "$@" && ls -F
    }
